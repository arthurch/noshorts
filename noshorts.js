var svgShort = '<svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" focusable="false" style="pointer-events: none; display: block; width: 100%; height: 100%;" class="style-scope yt-icon"><g width="24" height="24" viewBox="0 0 24 24" class="style-scope yt-icon"><g class="style-scope yt-icon"><path d="M17.77,10.32l-1.2-.5L18,9.06a3.74,3.74,0,0,0-3.5-6.62L6,6.94a3.74,3.74,0,0,0,.23,6.74l1.2.49L6,14.93a3.75,3.75,0,0,0,3.5,6.63l8.5-4.5a3.74,3.74,0,0,0-.23-6.74Z" fill="red" class="style-scope yt-icon"></path><polygon points="10 14.65 15 12 10 9.35 10 14.65" fill="#fff" class="style-scope yt-icon"></polygon></g></g></svg>'
var svgNoShort = '<svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" focusable="false" style="pointer-events: none; display: block; width: 100%; height: 100%;" class="style-scope yt-icon"><g width="24" height="24" viewBox="0 0 24 24" class="style-scope yt-icon"><g class="style-scope yt-icon"><path d="M17.77,10.32l-1.2-.5L18,9.06a3.74,3.74,0,0,0-3.5-6.62L6,6.94a3.74,3.74,0,0,0,.23,6.74l1.2.49L6,14.93a3.75,3.75,0,0,0,3.5,6.63l8.5-4.5a3.74,3.74,0,0,0-.23-6.74Z" fill="red" class="style-scope yt-icon"></path><polygon points="10 14.65 15 12 10 9.35 10 14.65" fill="#fff" class="style-scope yt-icon"></polygon><line x1="3" y1="3" x2="20" y2="20" style="stroke:rgb(0,0,0);stroke-width:2" /></g></g></svg>'

function f_svgShort()
{
    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svg.setAttribute("viewBox", "0 0 24 24");
    svg.setAttribute("preserveAspectRatio", "xMidYMid meet");
    svg.setAttribute("focusable", "false");
    svg.style.display = "block" 
    svg.style.width = "100%";
    svg.style.height= "100%"; 

    g2 = document.createElementNS("http://www.w3.org/2000/svg", "g");
    g1.className = "style-scope yt-icon";
    g1.setAttribute("viewBox", "0 0 24 24");
    g1.setAttribute("width", "24");
    g1.setAttribute("height", "24");
    svg.appendChild(g1);

    g2 = document.createElementNS("http://www.w3.org/2000/svg", "g");
    g2.className = "style-scope yt-icon";
    g1.appendChild(g2);

    p = document.createElementNS("http://www.w3.org/2000/svg", "path");
    p.setAttribute("d", "M17.77,10.32l-1.2-.5L18,9.06a3.74,3.74,0,0,0-3.5-6.62L6,6.94a3.74,3.74,0,0,0,.23,6.74l1.2.49L6,14.93a3.75,3.75,0,0,0,3.5,6.63l8.5-4.5a3.74,3.74,0,0,0-.23-6.74Z");
    p.setAttribute("fill", "red");
    p.className = "style-scope yt-icon";
    g2.appendChild(p);
    po = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
    po.setAttribute("points", "10 14.65 15 12 10 9.35 10 14.65");
    po.setAttribute("fill", "#fff");
    po.className = "style-scope yt-icon";
    g2.appendChild(po);

    return svg;
}

function f_svgNoShort()
{
    var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");    
    svg.setAttribute("viewBox", "0 0 24 24");
    svg.setAttribute("preserveAspectRatio", "xMidYMid meet");
    svg.setAttribute("focusable", "false");
    svg.style.display = "block" 
    svg.style.width = "100%";
    svg.style.height= "100%";

    g1 = document.createElementNS("http://www.w3.org/2000/svg", "g");
    g1.className = "style-scope yt-icon";
    g1.setAttribute("viewBox", "0 0 24 24");
    g1.setAttribute("width", "24");
    g1.setAttribute("height", "24");
    svg.appendChild(g1);

    g2 = document.createElementNS("http://www.w3.org/2000/svg", "g");
    g2.className = "style-scope yt-icon";
    g1.appendChild(g2);

    p = document.createElementNS("http://www.w3.org/2000/svg", "path");
    p.setAttribute("d", "M17.77,10.32l-1.2-.5L18,9.06a3.74,3.74,0,0,0-3.5-6.62L6,6.94a3.74,3.74,0,0,0,.23,6.74l1.2.49L6,14.93a3.75,3.75,0,0,0,3.5,6.63l8.5-4.5a3.74,3.74,0,0,0-.23-6.74Z");
    p.setAttribute("fill", "red");
    p.className = "style-scope yt-icon";
    g2.appendChild(p);
    po = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
    po.setAttribute("points", "10 14.65 15 12 10 9.35 10 14.65");
    po.setAttribute("fill", "#fff");
    po.className = "style-scope yt-icon";
    g2.appendChild(po);
    l = document.createElementNS("http://www.w3.org/2000/svg", "line");
    l.setAttribute("x1", "3");
    l.setAttribute("y1", "3");
    l.setAttribute("x2", "20");
    l.setAttribute("y2", "20");
    l.setAttribute("style", "stroke:rgb(0,0,0);stroke-width:2");
    l.className = "style-scope yt-icon";
    g2.appendChild(l);

    return svg;
}

function clearInner(node) {
    while (node.hasChildNodes()) {
        clear(node.firstChild);
    }
}

function clear(node) {
    while (node.hasChildNodes()) {
        clear(node.firstChild);
    }
    node.parentNode.removeChild(node);
}

var dlog = (s) => {
    // console.log("[noshorts] " + s);
};

var optionHideShort = true;
var optionChanged = true;

// option button
var createOption = () => {
    var btn = document.getElementById("noshortsoption");
    if(btn != null)
    {
        btn.style.display = "block";
        return;
    }

    btn = document.createElement('div');
    var updateOption = () => {
        clearInner(btn);

        if(optionHideShort)
        {
            btn.appendChild(f_svgNoShort());
        } else 
        {
            btn.appendChild(f_svgShort());
        }
    }
    updateOption();

    btn.style.position = "fixed";
    btn.style.width = "20px"
    btn.style.height = "20px"
    btn.style.zIndex = 100000;
    btn.style.top = 0;
    btn.style.cursor = "pointer";
    btn.style.left = "80%";
    btn.onclick = () => {
        optionHideShort = !optionHideShort;
        optionChanged = true;
        
        updateOption();
    };
    btn.id = "noshortsoption";
    document.body.appendChild(btn);
    dlog("just added the button");
}

var hideOptions = () => {
    var btn = document.getElementById("noshortsoption");
    if(btn != null)
    {
        btn.style.display = "none";
    }
}

var oldDay = 0;
lo = setInterval(() => {
    
    url = window.location.href;
    dlog("we are on "+ url);

    if(!url.includes("subscriptions"))
    {
        hideOptions();
        return;
    }

    createOption();

    dayDiv = null;
    possibleDays = document.querySelectorAll("#page-manager #primary #contents");
    for(d of possibleDays)
    {
        if(d.className.includes("ytd-section-list-renderer"))
        {
            dayDiv = d
        }
    }
    if(dayDiv == null) return;

    if(optionChanged) {
        optionChanged = false;
    }

    if(oldDay != dayDiv.children.length || optionChanged)
    {
        for(dayChild of dayDiv.children)
        {
            if(dayChild.tagName == "YTD-ITEM-SECTION-RENDERER" || dayChild.tagName == "YTD-RICH-SECTION-RENDERER")
            {
                // remove shorts from this list of videos
                videosDiv = dayChild.querySelector("#items");
                for(vid of videosDiv.children)
                {
                    if(vid.innerHTML.includes("SHORTS"))
                    {
                        if(optionHideShort)
                        {
                            vid.style.display = "none";
                        } else {
                            vid.style.display = "block";
                        }
                    }
                }
            }
        }
        
        dlog("removed shorts");
    }
}, 500);